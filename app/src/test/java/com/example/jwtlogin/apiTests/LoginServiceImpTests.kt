package com.example.jwtlogin.apiTests

import com.example.jwtlogin.api.LoginResponse
import com.example.jwtlogin.api.LoginServiceImp
import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.exceptions.UnauthorizedException
import com.example.jwtlogin.exceptions.UnknownException
import com.example.jwtlogin.services.interfaces.TokenService
import com.example.jwtlogin.testDi.testModule
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.MockResponse
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import org.junit.Assert
import org.koin.standalone.StandAloneContext
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.Mockito

class LoginServiceImpTests : KoinTest {

    lateinit var mockServer: MockWebServer
    private lateinit var sut: LoginServiceImp
    lateinit var testUserName: String
    lateinit var testUserPassword: String

    private val tokenService: TokenService by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(testModule))

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        mockServer = MockWebServer()
        mockServer.start()

        // Get an okhttp client
        val okHttpClient = OkHttpClient.Builder()
            .build()

        val serverUrl = mockServer.url("/")

        // Get an instance of Retrofit
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        sut = LoginServiceImp(
            retrofit.create(LoginService::class.java),
            tokenService
        )

        testUserName = "Test User"
        testUserPassword = "Password"
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
        stopKoin()
    }

    @Test
    fun `When Api Receives OK Response Then The Token Service Gets Saved To The Repository`() {
        val response = LoginResponse(
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZHA6dXNlcl9pZCI6IjUwY TdkYTFkLWZlMDctNGMxNC04YjFiLTAwNzczN2Y0Nzc2MyIsImlkcDp1c2VyX25h bWUiOiJqZG9lIiwiaWRwOmZ1bGxuYW1lIjoiSm9obiBEb2UiLCJyb2xlIjoiZWR pdG9yIiwiZXhwIjoxNTU2NDc2MjU1fQ.iqFmotBtfAYLplfpLVh_kPgvOIPyV7U Mm-NZA06XA5I",
            "bearer",
            119,
            "NTBhN2RhMWQtZmUwNy00YzE0LThiMWItMDA3NzM3ZjQ3NzYzIyNkNmQ5OTViZS"
        )
        Mockito.`when`(tokenService.save(any(LoginResponse::class.java)))
            .thenReturn(Single.just(response))

        val mockedResponse = MockResponse()
            .addHeader("Content-Type", "application/json; charset=utf-8")
            .addHeader("Cache-Control", "no-cache")
            .setBody(
                """{
                "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZHA6dXNlcl9pZCI6IjUwY TdkYTFkLWZlMDctNGMxNC04YjFiLTAwNzczN2Y0Nzc2MyIsImlkcDp1c2VyX25h bWUiOiJqZG9lIiwiaWRwOmZ1bGxuYW1lIjoiSm9obiBEb2UiLCJyb2xlIjoiZWR pdG9yIiwiZXhwIjoxNTU2NDc2MjU1fQ.iqFmotBtfAYLplfpLVh_kPgvOIPyV7U Mm-NZA06XA5I",
                "token_type": "bearer",
                "expires_in": 119,
                "refresh_token": "NTBhN2RhMWQtZmUwNy00YzE0LThiMWItMDA3NzM3ZjQ3NzYzIyNkNmQ5OTViZS"
                }"""

            )
            .setResponseCode(200)

        mockServer.enqueue(mockedResponse)
        val loginObservable = sut.login(testUserName, testUserPassword).test()

        Assert.assertEquals(loginObservable.values().last(), response)
    }

    @Test
    fun `When Api Service Receives 401 HTTP Error Than Response Contains Unauthorized Exception`() {
        val mockedResponse = MockResponse()
            .addHeader("Content-Type", "application/json; charset=utf-8")
            .addHeader("Cache-Control", "no-cache")
            .setBody(
                """{
                "error_message": "Authentication failed"
                }"""

            )
            .setResponseCode(401)

        mockServer.enqueue(mockedResponse)

        val response = sut.login("Test User", "Password").test()
        response.assertError(UnauthorizedException::class.java)
    }

    @Test
    fun `When Api Service Receives 500 HTTP Error Than Response Contains Unknown Exception`() {
        val mockedResponse = MockResponse()
            .addHeader("Content-Type", "application/json; charset=utf-8")
            .addHeader("Cache-Control", "no-cache")
            .setBody(
                """{
                "error_message": "Unknown error happened"
                }"""

            )
            .setResponseCode(500)

        mockServer.enqueue(mockedResponse)

        val response = sut.login("Test User", "Password").test()
        response.assertError(UnknownException::class.java)
    }

    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)

}
