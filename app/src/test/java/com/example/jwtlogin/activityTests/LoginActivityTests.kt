package com.example.jwtlogin.activityTests

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.text.InputType
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import com.example.jwtlogin.R
import com.example.jwtlogin.TestApplication
import com.example.jwtlogin.activities.HomeActivity
import com.example.jwtlogin.activities.LoginActivity
import com.example.jwtlogin.viewModels.LoginViewModel
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.Mockito
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog


@RunWith(RobolectricTestRunner::class)
@Config(application = TestApplication::class)
class LoginActivityTests : KoinTest {
    private lateinit var sut: LoginActivity
    private lateinit var passwordEditText: EditText
    private lateinit var showPasswordCheckBox: CheckBox
    private lateinit var loginButton: Button
    private val showPasswordChecked = MutableLiveData<Boolean>()
    private val navigateToHomeViewModel = MutableLiveData<Boolean>()
    private val loginButtonEnabled = ObservableField<Boolean>()
    private val alertMessage = MutableLiveData<String>()

    private val viewModel: LoginViewModel by inject()

    @Before
    fun setUp() {
        Mockito.`when`(viewModel.showPasswordChecked).thenReturn(showPasswordChecked)
        Mockito.`when`(viewModel.navigateToHomeViewModel).thenReturn(navigateToHomeViewModel)
        Mockito.`when`(viewModel.loginButtonEnabled).thenReturn(loginButtonEnabled)
        Mockito.`when`(viewModel.alertMessage).thenReturn(alertMessage)

        sut = Robolectric.buildActivity(LoginActivity::class.java)
            .create()
            .resume()
            .get()

        passwordEditText = sut.findViewById(R.id.passwordEditText)
        showPasswordCheckBox = sut.findViewById(R.id.showPasswordCheckBox)
        loginButton = sut.findViewById(R.id.loginButton)
    }

    @Test
    fun `Login Activity Is Not Null`() {
        assertNotNull(sut)
    }

    @Test
    fun `Login Activity Has UserNameEditText`() {
        assertNotNull(sut.findViewById(R.id.userNameEditText))
    }


    @Test
    fun `LoginActivity Has PasswordEditText`() {
        assertNotNull(sut.findViewById(R.id.passwordEditText))
    }

    @Test
    fun `LoginActivity Has ShowPasswordCheckBox`() {
        assertNotNull(sut.findViewById(R.id.showPasswordCheckBox))
    }

    @Test
    fun `Login Activity Password Field Type Is Password By Default`() {
        assertEquals(
            passwordEditText.inputType,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }


    @Test
    fun `LoginActivity PasswordEditText Does Not Show Password If ShowPasswordCheckBox Is NOT Checked`() {
        showPasswordChecked.postValue(true)

        assertEquals(
            passwordEditText.inputType,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }


    @Test
    fun `LoginActivity PasswordEditText Shows Password If ShowPasswordCheckBox Is Checked`() {
        showPasswordChecked.postValue(false)

        assertEquals(
            passwordEditText.inputType,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        )
    }

    @Test
    fun `LoginActivity Has LoginButton`() {
        assertNotNull(sut.findViewById(R.id.loginButton))
    }

    @Test
    fun `When View Model Requires Login Button Disabled Than Login Button Is Disabled`() {
        assertFalse(sut.viewModel.loginButtonEnabled.get()!!)
        assertFalse(loginButton.isEnabled)
    }

    @Test
    fun `When View Model Requires Login Button Enabled Than Login Button Is Enabled`() {
        loginButtonEnabled.set(true)

        assertTrue(sut.viewModel.loginButtonEnabled.get()!!)
        assertTrue(loginButton.isEnabled)
    }

    @Test
    fun `When View Model Requires Navigation To To Home Screen Than Start Activity Gets Called`() {
        navigateToHomeViewModel.postValue(true)

        val startedIntent = shadowOf(sut).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)

        assertEquals(HomeActivity::class.java, shadowIntent.intentClass)
    }

    @Test
    fun `When View Model Requires Alert Message Showing Than Alert Message Appears`() {
        val errorMessage = "Error happened"
        alertMessage.postValue(errorMessage)

        val alertDialog = ShadowAlertDialog.getLatestAlertDialog()
        val shadowAlertDialog = shadowOf(alertDialog)
        assertEquals(shadowAlertDialog.message, errorMessage)
    }
}