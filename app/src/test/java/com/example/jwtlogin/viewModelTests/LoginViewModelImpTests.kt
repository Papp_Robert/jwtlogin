package com.example.jwtlogin.viewModelTests

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.databinding.ObservableField
import com.example.jwtlogin.api.LoginResponse
import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.exceptions.UnauthorizedException
import com.example.jwtlogin.services.interfaces.NetworkManager
import com.example.jwtlogin.testDi.testModule
import com.example.jwtlogin.viewModels.LoginViewModelImp
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.*
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.*

class LoginViewModelImpTests : KoinTest {

    private lateinit var sut: LoginViewModelImp
    private val loginService: LoginService by inject()
    private val networkManager: NetworkManager by inject()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        startKoin(listOf(testModule))
        sut = LoginViewModelImp(
            loginService,
            networkManager
        )

        sut.password.set("Password")
        sut.userName.set("UserName")

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun `LoginViewModel Has UserNameText Property`() {
        Assert.assertNotNull(sut.userName)
    }

    @Test
    fun `LoginViewModel Has PasswordText Property`() {
        Assert.assertNotNull(sut.password)
    }

    @Test
    fun `LoginViewModel Has LoginButtonEnabled Property`() {
        Assert.assertNotNull(sut.loginButtonEnabled.get())
    }

    @Test
    fun `Login Button Is Enabled When UserName And Password Fields Are Not Empty`() {
        sut.password.set("UserName")
        sut.userName.set("Password")

        Assert.assertEquals(sut.loginButtonEnabled.get(), true)
    }

    @Test
    fun `Login Button Disabled When UserName Is Empty And Password Field Is Filled`() {
        sut.password.set("Password")
        sut.userName.set("")

        Assert.assertEquals(sut.loginButtonEnabled.get(), false)
    }

    @Test
    fun `Login Button Is Disabled When UserName Is Not Empty And Password Field Is Empty`() {
        sut.userName.set("UserName")
        sut.userName.notifyChange()

        sut.password.set("")
        sut.password.notifyChange()

        Assert.assertEquals(sut.loginButtonEnabled.get(), false)
    }

    @Test
    fun `When Login Button Tapped A Loading Indicator Is Showed And Hidden`() {
        Assert.assertEquals(sut.loading, false)

        sut.login()
        Assert.assertEquals(sut.loading, true)
    }

    @Test
    fun `When Login Button Tapped And The Response Contains Unauthorized Error Then Alert Message Gets Manipulated With Error Message`() {
        val errorMessage = "Invalid username or password."

        mockNetworkManagerWithAvailableNetwork(true)

        Mockito.`when`(loginService.getAccessToken(anyString(), anyString(), anyString(), anyString())).thenReturn(
            Single.error(UnauthorizedException(errorMessage))
        )

        sut.login()

        Assert.assertEquals(sut.alertMessage.value, errorMessage)
    }

    @Test
    fun `When Login Button Tapped And The Response Contains Unknown Error Then Alert Message Gets Unknown Error Message`() {
        val errorMessage = "Unknown error happened"

        Mockito.`when`(loginService.getAccessToken(anyString(), anyString(), anyString(), anyString())).thenReturn(
            Single.error(UnknownError(errorMessage))
        )

        Mockito.`when`(networkManager.isNetworkConnected()).thenReturn(
            Single.just(true)
        )

        sut.login()

        Assert.assertEquals(sut.alertMessage.value, errorMessage)
    }

    @Test
    fun `When Login Button Tapped With Empty Parameters And The Response Contains Unknown Error Then Alert Message Gets Unknown Error Message`() {
        val errorMessage = "Unknown error happened"

        Mockito.`when`(loginService.getAccessToken(anyString(), anyString(), anyString(), anyString())).thenReturn(
            Single.error(UnknownError(errorMessage))
        )

        mockNetworkManagerWithAvailableNetwork(true)

        sut.login()

        Assert.assertEquals(sut.alertMessage.value, errorMessage)
    }

    @Test
    fun `When Login Button Tapped And The Response Contains Well Formatted Response Then App Navigate To Home View Model Gets Called`() {
        mockNetworkManagerWithAvailableNetwork(true)

        Mockito.`when`(loginService.getAccessToken(anyString(), anyString(), anyString(), anyString())).thenReturn(
            Single.just(LoginResponse("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZHA6dXNlcl9pZCI6IjUwY TdkYTFkLWZlMDctNGMxNC04YjFiLTAwNzczN2Y0Nzc2MyIsImlkcDp1c2VyX25h bWUiOiJqZG9lIiwiaWRwOmZ1bGxuYW1lIjoiSm9obiBEb2UiLCJyb2xlIjoiZWR pdG9yIiwiZXhwIjoxNTU2NDc2MjU1fQ.iqFmotBtfAYLplfpLVh_kPgvOIPyV7U Mm-NZA06XA5I",
                "bearer",
                119,
                "NTBhN2RhMWQtZmUwNy00YzE0LThiMWItMDA3NzM3ZjQ3NzYzIyNkNmQ5OTViZS"))
        )

        sut.login()

        Assert.assertEquals(sut.navigateToHomeViewModel.value, true)
    }

    @Test
    fun `When There Is No Network Then The User Sees An Alert Message`() {
        mockNetworkManagerWithAvailableNetwork(false)

        sut.login()

        Assert.assertEquals(sut.alertMessage.value,
            "Connection broken. Verify that you are connected to the internet.")
    }

    @Test
    fun `When There Is Network Available Then The Get Access Token Gets Called One Time`() {
        mockNetworkManagerWithAvailableNetwork(true)

        sut.login()

        verify(loginService, times(1)).getAccessToken(anyString(), anyString(), anyString(), anyString())
    }

    private fun mockNetworkManagerWithAvailableNetwork(networkAvailable: Boolean) {
        Mockito.`when`(networkManager.isNetworkConnected()).thenReturn(
            Single.just(networkAvailable)
        )
    }

}