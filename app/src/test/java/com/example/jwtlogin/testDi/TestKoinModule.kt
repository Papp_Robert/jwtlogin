package com.example.jwtlogin.testDi

import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.services.interfaces.NetworkManager
import com.example.jwtlogin.services.interfaces.TokenService
import com.example.jwtlogin.viewModels.LoginViewModel
import org.koin.dsl.module.module
import org.mockito.Mockito

val testModule = module {

    single { Mockito.mock(LoginService::class.java) }

    single { Mockito.mock(TokenService::class.java) }

    single { Mockito.mock(NetworkManager::class.java) }

    single { Mockito.mock(LoginViewModel::class.java) }
}