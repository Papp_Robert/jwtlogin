package com.example.jwtlogin

import android.app.Application
import com.example.jwtlogin.testDi.testModule
import org.koin.standalone.StandAloneContext
import org.koin.standalone.StandAloneContext.stopKoin
import org.robolectric.TestLifecycleApplication
import java.lang.reflect.Method

class TestApplication : Application(), TestLifecycleApplication {
    override fun onCreate() {
        super.onCreate()
    }

    override fun beforeTest(method: Method?) {
        StandAloneContext.startKoin(listOf(testModule))
    }

    override fun prepareTest(test: Any?) {
    }

    override fun afterTest(method: Method?) {
        stopKoin()
    }
}