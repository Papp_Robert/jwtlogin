package com.example.jwtlogin.modelTests

import com.example.jwtlogin.models.User
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UserTests {

    @Test
    fun `User access token is set to Token value`() {
        val user = User("Token")

        assertEquals(user.accessToken, "Token")
    }

    @Test
    fun `User access token has value than authorized is true`() {
        val user = User("Token")

        assertEquals(user.authorized, true)
    }

    @Test
    fun `User access token has no value than authorized is false`() {
        val user = User("")

        assertEquals(user.authorized, false)
    }

}
