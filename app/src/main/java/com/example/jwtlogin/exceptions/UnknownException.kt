package com.example.jwtlogin.exceptions

import java.lang.Exception

class UnknownException(errorMessage: String) : Exception(errorMessage)