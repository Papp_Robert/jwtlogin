package com.example.jwtlogin.exceptions

import java.lang.Exception

class UnauthorizedException(exceptionMessage: String) : Exception(exceptionMessage)