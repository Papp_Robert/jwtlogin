package com.example.jwtlogin.exceptions

import java.lang.Exception

class InvalidParametersException(errorMessage: String) : Exception(errorMessage)