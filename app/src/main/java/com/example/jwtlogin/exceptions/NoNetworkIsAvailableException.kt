package com.example.jwtlogin.exceptions

import java.lang.Exception

class NoNetworkIsAvailableException(exceptionMessage: String) : Exception(exceptionMessage)