package com.example.jwtlogin.models

data class User(var accessToken: String) {
    val authorized: Boolean
            get() = accessToken.isNotEmpty()
}