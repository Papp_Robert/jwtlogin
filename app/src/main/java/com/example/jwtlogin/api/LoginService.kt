package com.example.jwtlogin.api

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginService {
    companion object {
        fun create(): LoginService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://example.vividmindsoft.com/")
                .build()

            return retrofit.create(LoginService::class.java)
        }
    }

    @POST("idp/api/v1/token")
    fun getAccessToken(
        @Query("username") username: String,
        @Query("password") password: String,
        @Query("grant_type") grantType: String = "password",
        @Query("client_id") clientId: String = "clientId"
    ): Single<LoginResponse>
}
