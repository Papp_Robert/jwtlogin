package com.example.jwtlogin.api

import com.example.jwtlogin.exceptions.UnauthorizedException
import com.example.jwtlogin.exceptions.UnknownException
import com.example.jwtlogin.services.interfaces.TokenService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class LoginServiceImp(
    private val loginService: LoginService,
    var tokenService: TokenService
) {

    fun login(
        username: String, password: String, grantType: String = "password",
        clientId: String = "clientId"
    ): Single<LoginResponse> {
        return loginService.getAccessToken(username, password, grantType, clientId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap {
                tokenService.save(it)
            }
            .onErrorResumeNext {
                Single.error(if (it is HttpException && it.code() == 401) {
                    UnauthorizedException("Invalid username or password")

                } else {
                    UnknownException("Unknown exception happened")
                })
            }
    }
}