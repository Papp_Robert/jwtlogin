package com.example.jwtlogin.api

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("access_token")
    var accessToken: String = "",
    @SerializedName("token_type")
    var tokenType: String = "",
    @SerializedName("expires_in")
    var expiresIn: Int = 0,
    @SerializedName("refresh_token")
    var refreshToken: String = ""
)