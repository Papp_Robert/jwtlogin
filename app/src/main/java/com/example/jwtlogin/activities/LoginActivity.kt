package com.example.jwtlogin.activities

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import com.example.jwtlogin.R
import com.example.jwtlogin.databinding.ActivityLoginBinding
import com.example.jwtlogin.viewModels.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {

    // Lazy Inject ViewModel
    val viewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        binding.viewModel = viewModel

        handleShowPasswordCheckedValueChanged()

        viewModel.navigateToHomeViewModel.observe(this, Observer<Boolean> {
            if (it!!) {
                val homeActivity = Intent(this@LoginActivity, HomeActivity::class.java)
                startActivity(homeActivity)
            }
        })

        viewModel.alertMessage.observe(this, Observer<String> {
            AlertDialog.Builder(this)
                .setTitle("Unexpected error happened")
                .setMessage(it).show()
        })
    }

    private fun handleShowPasswordCheckedValueChanged() {
        viewModel.showPasswordChecked.observe(this, Observer<Boolean> {
            if (it!!) {
                setPasswordEditTextToShowPassword()
            } else {
                setPasswordEditTextToHidePassword()
            }
        })
    }

    private fun setPasswordEditTextToHidePassword() {
        passwordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
    }

    private fun setPasswordEditTextToShowPassword() {
        passwordEditText.transformationMethod = HideReturnsTransformationMethod.getInstance()
    }
}
