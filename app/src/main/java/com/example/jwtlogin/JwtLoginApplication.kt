package com.example.jwtlogin

import android.app.Application
import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.di.appModule
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin

open class JwtLoginApplication : Application() {

    val loginService: LoginService by inject()

    override fun onCreate() {
        super.onCreate()

        // Start Koin
        startKoin(this, listOf(appModule))
    }
}