package com.example.jwtlogin.di

import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.services.implementation.NetworkManagerImp
import com.example.jwtlogin.services.implementation.TokenServiceImp
import com.example.jwtlogin.services.interfaces.NetworkManager
import com.example.jwtlogin.services.interfaces.TokenService
import com.example.jwtlogin.viewModels.LoginViewModel
import com.example.jwtlogin.viewModels.LoginViewModelImp
import org.koin.dsl.module.module

val appModule = module {

    // LoginActivity's ViewModel
    single { LoginViewModelImp(get(), get()) as LoginViewModel }

    single { LoginService.create() }

    single { TokenServiceImp() as TokenService }

    single { NetworkManagerImp() as NetworkManager }
}