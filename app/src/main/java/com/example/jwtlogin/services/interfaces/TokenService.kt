package com.example.jwtlogin.services.interfaces

import com.example.jwtlogin.api.LoginResponse
import io.reactivex.Single

interface TokenService {

    fun save(loginResponse: LoginResponse) : Single<LoginResponse>

    fun getLatest() : Single<LoginResponse>
}