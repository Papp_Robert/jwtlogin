package com.example.jwtlogin.services.interfaces

import io.reactivex.Single

interface NetworkManager {
    fun isNetworkConnected(): Single<Boolean>
}