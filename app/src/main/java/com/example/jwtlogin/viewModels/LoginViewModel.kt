package com.example.jwtlogin.viewModels

import android.arch.lifecycle.LiveData
import android.databinding.ObservableField
import android.widget.CompoundButton

interface LoginViewModel {

    val loginButtonEnabled: ObservableField<Boolean>

    val password: ObservableField<String>

    val userName: ObservableField<String>

    val loading: ObservableField<Boolean>

    val showPasswordChecked: LiveData<Boolean>

    val alertMessage: LiveData<String>

    val navigateToHomeViewModel: LiveData<Boolean>

    fun showPasswordChanged(button: CompoundButton, isChecked: Boolean)

    fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int)

    fun onUserNameChanged(s: CharSequence, start: Int, befor: Int, count: Int)

    fun login()
}