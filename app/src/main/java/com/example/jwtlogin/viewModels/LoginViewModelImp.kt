package com.example.jwtlogin.viewModels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import android.widget.CompoundButton
import com.example.jwtlogin.api.LoginService
import com.example.jwtlogin.exceptions.InvalidParametersException
import com.example.jwtlogin.exceptions.NoNetworkIsAvailableException
import com.example.jwtlogin.services.interfaces.NetworkManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModelImp(
    private val loginService: LoginService,
    private val networkManager: NetworkManager
) : ViewModel(), LoginViewModel {

    override val loginButtonEnabled: ObservableField<Boolean> = ObservableField()

    override val password: ObservableField<String> = ObservableField()

    override val userName: ObservableField<String> = ObservableField()

    override val loading: ObservableField<Boolean> = ObservableField()

    override val showPasswordChecked: MutableLiveData<Boolean> = MutableLiveData()

    override val alertMessage: MutableLiveData<String> = MutableLiveData()

    override val navigateToHomeViewModel: MutableLiveData<Boolean> = MutableLiveData()

    override fun showPasswordChanged(button: CompoundButton, isChecked: Boolean) {
        showPasswordChecked.postValue(isChecked)
    }

    override fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        loginButtonEnabled.set(getLoginButtonVisibility())
    }

    override fun onUserNameChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        loginButtonEnabled.set(getLoginButtonVisibility())
    }

    override fun login() {
        loading.set(true)

        networkManager.isNetworkConnected().flatMap {
            if (it) {
                val userNameIsNotEmpty = !userName.get().isNullOrEmpty()
                val passwordIsNotEmpty = !password.get().isNullOrEmpty()

                if (userNameIsNotEmpty.and(passwordIsNotEmpty)) {
                    loginService.getAccessToken(userName.get()!!, password.get()!!)
                } else {
                    Single.error(InvalidParametersException("The parameters are not valid."))
                }
            } else {
                Single.error(NoNetworkIsAvailableException("Connection broken. Verify that you are connected to the internet."))
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    navigateToHomeViewModel.postValue(true)

                    loading.set(false)
                },
                {
                    alertMessage.postValue(it.message)

                    loading.set(false)
                }
            )
    }

    private fun getLoginButtonVisibility(): Boolean {
        return !password.get().isNullOrEmpty().and(!userName.get().isNullOrEmpty())
    }
}